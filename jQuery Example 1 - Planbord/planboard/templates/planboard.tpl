{include file="planboard/components/header.tpl"}
{include file="planboard/components/error.tpl"}
<div class="columnWrapper container12">
    <div class="draggable">
        <div class="column column2" data-status="backlog">
            <h2><span class="statusColor statusBacklog"></span>Backlog</h2>
            {foreach from=$backlogTasks item=task}
                {include file="planboard/task/task.tpl"}
            {/foreach}
            <div class="addTask columnSubtext">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add task to project
            </div>
        </div>
        <div class="column column2" data-status="todo">
            <h2><span class="statusColor statusTodo"></span>To do</h2>
            {foreach from=$todoTasks item=task}
                {include file="planboard/task/task.tpl"}
            {/foreach}
        </div>
        <div class="column column2" data-status="doing">
            <h2><span class="statusColor statusDoing"></span>Doing</h2>
            {foreach from=$doingTasks item=task}
                {include file="planboard/task/task.tpl"}
            {/foreach}

        </div>
        <div class="column column2" data-status="woa">
            <h2><span class="statusColor statusWOA"></span>Waiting on answer</h2>
            {foreach from=$woaTasks item=task}
                {include file="planboard/task/task.tpl"}
            {/foreach}
        </div>
        <div class="column column2 noDrag" data-status="done">
            <h2><span class="statusColor statusDone"></span>Done</h2>
            {foreach from=$doneTasks item=task}
                {include file="planboard/task/task.tpl"}
            {/foreach}
            <div class="columnSubtext">
                <div class="showEditFirstDay">
                    <div class="currentDay">Since <span class="firstDayOfThisWeek"></span> <i
                                class="fa fa-pencil" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="editFirstDay"><span>Since</span>
                    {if ("today"|strtotime|date_format:'D' != Mon)}
                        <input name="done_since" class="done_since" type="date"
                               value="{"last Monday"|strtotime|date_format:'Y-m-d'}">
                    {else}
                        <input name="done_since" class="done_since" type="date"
                               value="{"today"|strtotime|date_format:'Y-m-d'}">
                    {/if}
                    <div class="hide hideEditFirstDay"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="column column2 filterColumn">
        <div class="btn addTaskBtn addTask">
            <i class="fa fa-plus" aria-hidden="true"></i> Add task to project
        </div>
        <form method="POST" id="filterForm">
            <span class="select2Arrows filterSelect">
                <select name="filterEmployee" title="employee_id" class="select2 filter" data-filter="employee">
                    <option selected value="all">All employees</option>
                    {foreach from=$employees item=employee}
                        <option value={$employee->id} {if (isset($smarty.post.employee_id) && $smarty.post.employee_id=="{$employee->id}")}"selected"{/if}>{$employee->getFullName()|ucfirst}
                        </option>
                    {/foreach}
                </select>
            </span>

            <span class="select2Arrows filterSelect">
                <select name="filterCompany" title="company_id" class="select2 filter" data-filter="company">
                    <option selected value="all">All companies</option>
                    {foreach from=$companies item=company}
                        <option value={$company->id} {if (isset($smarty.post.company_id) && $smarty.post.company_id=="{$company->id}")}"selected"{/if}>{$company->getName()}
                        </option>
                    {/foreach}
                </select>
            </span>

            <span class="select2Arrows filterSelect">
                <select name="filterProject" title="project_id" class="select2 filter" data-filter="project">
                    <option selected value="all">All projects</option>
                    {foreach from=$projects item=project}
                        <option value={$project->id} {if (isset($smarty.post.project_id) && $smarty.post.project_id=="{$project->id}")}"selected"{/if}>{$project->getName()|ucfirst}
                        </option>
                    {/foreach}
                </select>
            </span>

            <span class="select2Arrows filterSelect">
                <select name="filterEpic" title="epic_id" class="select2 filter" data-filter="epic">
                    <option selected value="all">All epics</option>
                    {foreach from=$epics item=epic}
                        <option value={$epic->id} {if (isset($smarty.post.epic_id) && $smarty.post.epic_id=="{$epic->id}")}"selected"{/if}>{$epic->getTitle()|ucfirst}
                        </option>
                    {/foreach}
                </select>
            </span>

            <span class="select2Arrows filterSelect">
                <select name=filterTicket" title="ticket_id" class="select2 filter" data-filter="ticket">
                    <option selected value="all">All tickets</option>
                    {foreach from=$tickets item=ticket}
                        <option value={$ticket->id} {if (isset($smarty.post.ticket_id) && $smarty.post.ticket_id=="{$ticket->id}")}"selected"{/if}>{$ticket->getTitle()|ucfirst}
                        </option>
                    {/foreach}
                </select>
            </span>
        </form>
        <div class="clearFilter columnSubtext">
            clear filter
        </div>
    </div>
</div>
<div class="modalContainer"></div>
{include file="planboard/components/footer.tpl"}