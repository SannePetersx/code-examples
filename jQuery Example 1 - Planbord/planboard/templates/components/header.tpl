<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Planboard > WebIQ</title>
    <link rel="stylesheet" href="/css/vendor.css">
    <link rel="stylesheet" href="/css/planboard.css">
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/vendor.js"></script>
    <script type="text/javascript" src="{$socketIoUrl}"></script>
    <script type="text/javascript" src="/js/planboard.js"></script>
    <link href="/images/favicon.ico" rel="shortcut icon">
</head>
<body>
<header>
    <div class="container12">
        <div class="column2 logoContainer"><a href="/" class="logo"></a></div>
        <div class="loadingIcon"></div>
        <div class="loggedIn column2" data-userid="{$user->id}" data-username="{$user->getFullName()}">
            <div class="userIcon"></div>
            <span>{$user->getFullName()}</span>
        </div>
    </div>
</header>