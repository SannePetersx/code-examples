<div class="task" data-id="{$task->id}" data-status="{$task->getStatus()}">
    <div class="mainView">
        <div class="title">
            <div class="taskName">{$task->getTitle()|ucfirst}</div>
            <div class="companyName">{$task->getCompany()->getName()|ucfirst}</div>
        </div>
        <div class="attributes">
            <div class="difficulty">
                {if ($task->getDifficulty())}
                    {$task->getDifficulty()|upper}
                {/if}
            </div>
            {if ($task->getEstimatedTime() != '00:00')}
                <div class="estimatedTime">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    {$task->getEstimatedTime()|date_format:'%H:%M'}
                </div>
            {/if}
            {if ($task->getTimeSpend() != '00:00' )}
                <div class="timeSpend">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    {$task->getTimeSpend()|date_format:'%H:%M'}
                </div>
            {/if}
            <div class="initials">
                {if ($task->getAssignedTo())}
                    {$task->getAssignedTo()->getInitials()|ucfirst}
                {/if}
            </div>
        </div>
    </div>
    <div class="data">
        <div>
            {if ($task->getCreatedBy())}
                <span class="leftSpan">Created by:</span>
                <span class="rightSpan">{$task->getCreatedBy()->getFirstName()}</span>
            {/if}
        </div>
        <div>
            <span class="leftSpan">Created at:</span>
            <span class="rightSpan">{$task->getCreatedAt()|date_format:'d M Y'}</span>
        </div>
        {if ($task->getDueDate())}
            <div>
                <span class="leftSpan">Due date:</span>
                <span class="rightSpan">{$task->getDueDate()|date_format:'d M Y'}</span>
            </div>
        {/if}
        {if ($task->getClosedAt() != '—' && $task->getClosedAt())}
            <div class="closedAT">
                <span class="leftSpan">Closed at:</span>
                <span class="rightSpan">{$task->getClosedAt()|date_format:'d M Y'}</span>
            </div>
        {/if}
        <div class="closedAT">
            <span class="leftSpan">Billable:</span>
            <span class="rightSpan">{if ($task->isBillable())}Yes{else}No{/if}</span>
        </div>

        <div class="taskType">
            <a href="{if ($task->getType() == 'Epic')}project/{$task->getEpic()->getProject()->id}{else}ticket/{$task->getTicket()->id}{/if}">{if ($task->getType() == 'Epic')}Project{else}{$task->getType()}{/if}</a>
        </div>
        {if ($task->getDescription())}
            <div class="description">
                {$task->getDescription()}
            </div>
        {/if}
        <div class="taskIcons">
            {if ($task->getStatus() == 'backlog')}
                <div class="deleteTask" data-id="{$task->id}" data-name="{$task->getTitle()}">
                    <i class="fa fa-trash"></i></div>
            {/if}
            {if ($task->getStatus() != 'done')}
                <div class="editTask" data-id="{$task->id}">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </div>
            {/if}
        </div>
    </div>
    <div class="showOrHide show">
        more <i class="fa fa-angle-down" aria-hidden="true"></i>
    </div>
</div>