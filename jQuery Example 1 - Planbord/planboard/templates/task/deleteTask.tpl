<div class="overlay deleteTaskModal">
    <div class="modal">
        <div class="header">
            <h2>Remove task</h2>
            <div class="hideModal"></div>
        </div>
        {include file="planboard/components/modalerror.tpl"}
        <p>Do you really want to remove the following task.</p>

        <div class="deleteTaskName">#{$task->id} {$task->getTitle()}</div>

        <div class="exitForm">
            <div class="btn save" data-id="{$task->id}">Confirm</div>
            <div class="btn cancel">Cancel</div>
        </div>
    </div>
</div>