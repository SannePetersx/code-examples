<div class="overlay editTaskModal">
    <div class="modal">
        <div class="header">
            <h2>Edit task</h2>
            <div class="hideModal"></div>
        </div>
        {include file="planboard/components/modalerror.tpl"}
        <form class="editTaskForm" onsubmit="event.preventDefault();">
            <div class="flexTable">
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="title">Title<span class="requiredField">*</span></label>
                    </div>
                    <div class="cell formString">
                        {$task->getTitle()|ucfirst}
                    </div>
                </div>
                {if $task->getType() == 'Ticket'}
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="ticket_id">Ticket<span
                                        class="requiredField">*</span></label>
                        </div>
                        <div class="cell formString">
                            {$task->getTicket()->getTitle()|ucfirst}
                        </div>
                    </div>
                {elseif $task->getType() == 'Epic'}
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="project_id">Project<span
                                        class="requiredField">*</span></label>
                        </div>
                        <div class="cell formString">
                            {$task->getEpic()->getProject()->getName()|ucfirst}
                        </div>
                    </div>
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="epic_id">Epic<span
                                        class="requiredField">*</span></label>
                        </div>
                        <div class="cell formString">
                            {$task->getEpic()->getTitle()|ucfirst}
                        </div>
                    </div>
                {/if}
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="due_date">Due date</label>
                    </div>
                    <div class="cell">
                        <input name="due_date" type="date" value="{$task->getDueDate()|date_format:'Y-m-d'}">
                    </div>
                </div>
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="billable">Billable</label>
                    </div>

                    <div class="cell">
                        <input name="billable" class="forCheckbox" type="checkbox"
                               {if ($task->isBillable())}checked{/if}>
                    </div>
                </div>
                <div class="body-row row">
                    <div class="cell column">
                        <label for="description">Description</label><br>
                        <textarea
                                name="description">{if isset($smarty.post.description)}{$smarty.post.description}{else}{$task->getDescription()}{/if}</textarea>
                    </div>
                </div>

                <h2>Assignee <span>(optional)</span></h2>

                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="assignee">Employee</label>
                    </div>
                    <div class="cell">
                        <span class="select2Arrows">
                            <select name="assignee" id="employeeSelector" class="select2"
                                    {if ($task->getStatus() == 'done')}disabled{/if}>
                                <option selected disabled>Select an employee</option>
                                {foreach from=$employees item=employee}
                                    <option value={$employee->id} {if isset($task->getAssignedTo()->id) && $task->getAssignedTo()->id == "{$employee->id}"}selected{/if}>{$employee->getFullName()|ucfirst}</option>
                                {/foreach}
                            </select>
                        </span>
                    </div>
                </div>
                <div class="onlyShowIfAssigneeIsYou" data-id="{$user->id}">
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="difficulty">Difficulty</label>
                        </div>
                        <div class="cell">
                       <span class="selectArrows">
                            <select name="difficulty"
                                    {if ($task->getStatus() == 'done' || $task->getStatus() == 'doing' || $task->getStatus() == 'woa')}disabled{/if}>
                                <option disabled selected value></option>
                                <option value="xs"
                                        {if ($task->getDifficulty() && $task->getDifficulty() == "xs")}selected{/if}>
                                    XS
                                </option>
                                <option value="s"
                                        {if ($task->getDifficulty() && $task->getDifficulty() == "s")}selected{/if}>
                                    S
                                </option>
                                <option value="m"
                                        {if ($task->getDifficulty() && $task->getDifficulty() == "m")}selected{/if}>
                                    M
                                </option>
                                <option value="l"
                                        {if ($task->getDifficulty() && $task->getDifficulty() == "l")}selected{/if}>
                                    L
                                </option>
                                <option value="xl"
                                        {if ($task->getDifficulty() && $task->getDifficulty() == "xl")}selected{/if}>
                                    XL
                                </option>
                            </select>
                       </span>
                        </div>
                    </div>
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="estimated_time">Estimated time</label>
                        </div>
                        <div class="cell">
                            <input name="estimated_time" type="time" placeholder="00:00"
                                   {if ($task->getStatus() == 'done' || $task->getStatus() == 'doing' || $task->getStatus() == 'woa')}disabled{/if}
                                   value="{if $task->getEstimatedTime() && $task->getEstimatedTime() > 0}{$task->getEstimatedTime()|date_format:'%H:%M'}{/if}">
                        </div>
                    </div>
                    {if ($task->getStatus() != 'backlog' && $task->getStatus() != 'todo')}
                        <div class="body-row row alignBaseline">
                            <div class="cell">
                                <label for="time_spend">Time spend: </label>
                            </div>
                            <div class="cell">
                                <input name="time_spend" type="time" placeholder="00:00"
                                       {if ($task->getStatus() == 'done')}disabled{/if}
                                       value="{if ($task->getTimeSpend()) && $task->getTimeSpend() > 0}{$task->getTimeSpend()|date_format:'%H:%M'}{/if}">
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
            <div class="exitForm">
                <input type="submit" class="btn save" value="Save" data-id="{$task->id}"
                       data-status="{$task->getStatus()}">
                <div class="btn cancel">Cancel</div>
            </div>
        </form>
    </div>
</div>