<div class="overlay addTaskModal">
    <div class="modal">
        <div class="header">
            <h2>Add task to project</h2>
            <div class="hideModal"></div>
        </div>
        {include file="planboard/components/modalerror.tpl"}
        <form method="POST" class="addTaskForm" onsubmit="event.preventDefault();">
            <div class="flexTable">
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="title">Title<span
                                    class="requiredField">*</span></label>
                    </div>
                    <div class="cell">
                        <input name="title" type="text" maxlength="80" value="{$smarty.post.title}"
                               required>
                    </div>
                </div>
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="project_id">Project<span
                                    class="requiredField">*</span></label>
                    </div>
                    <div class="cell">
                        <span class="select2Arrows">
                            <select name="project_id" class="select2" onchange="getEpics(this)" required>
                                <option selected disabled>Select a project</option>
                                {foreach from=$projects item=project}
                                    <option value={$project->id} {if (isset($smarty.post.project_id) && $smarty.post.project_id=="{$project->id}")}"selected"{/if}>{$project->getName()|ucfirst}</option>
                                {/foreach}
                            </select>
                        </span>
                    </div>
                </div>
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="epic_id">Epic<span
                                    class="requiredField">*</span></label>
                    </div>
                    <div class="cell selectEpic" id="epics">
                        <span class="text">Select a project first.</span>
                    </div>
                </div>
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="due_date">Due date</label>
                    </div>
                    <div class="cell">
                        <input name="due_date" type="date" value="{$smarty.post.due_date}">
                    </div>
                </div>
                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="billable">Billable</label>
                    </div>
                    <div class="cell">
                        <input name="billable" class="forCheckbox" type="checkbox">
                    </div>
                </div>
                <div class="body-row row">
                    <div class="cell column">
                        <label for="description">Description</label><br>
                        <textarea name="description">{$smarty.post.description}</textarea>
                    </div>
                </div>

                <h2>Assignee <span>(optional)</span></h2>

                <div class="body-row row alignBaseline">
                    <div class="cell">
                        <label for="assignee">Employee</label>
                    </div>
                    <div class="cell">
                        <span class="select2Arrows">
                            <select name="assignee" id="employeeSelector" class="select2">
                                <option selected disabled>Select an employee</option>
                                {foreach from=$employees item=employee}
                                    <option value={$employee->id} {if (isset($smarty.post.employee_id) && $smarty.post.employee_id=="{$employee->id}")}"selected"{/if}>{$employee->getFullName()|ucfirst}</option>
                                {/foreach}
                            </select>
                        </span>
                    </div>
                </div>
                <div class="onlyShowIfAssigneeIsYou" data-id="{$user->id}">
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="difficulty">Difficulty</label>
                        </div>
                        <div class="cell">
                       <span class="selectArrows">
                            <select name="difficulty">
                                <option disabled selected value></option>
                                <option value="xs"
                                        {if ($smarty.post.difficulty && $smarty.post.difficulty == "xs")}selected{/if}>
                                    XS
                                </option>
                                <option value="s"
                                        {if ($smarty.post.difficulty && $smarty.post.difficulty == "s")}selected{/if}>
                                    S
                                </option>
                                <option value="m"
                                        {if ($smarty.post.difficulty && $smarty.post.difficulty == "m")}selected{/if}>
                                    M
                                </option>
                                <option value="l"
                                        {if ($smarty.post.difficulty && $smarty.post.difficulty == "l")}selected{/if}>
                                    L
                                </option>
                                <option value="xl"
                                        {if ($smarty.post.difficulty && $smarty.post.difficulty == "xl")}selected{/if}>
                                    XL
                                </option>
                            </select>
                       </span>
                        </div>
                    </div>
                    <div class="body-row row alignBaseline">
                        <div class="cell">
                            <label for="estimated_time">Estimated time</label>
                        </div>
                        <div class="cell">
                            <input name="estimated_time" type="time" placeholder="00:00"
                                   value="{$smarty.post.estimated_time}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="exitForm">
                <input type="submit" class="btn save" value="Save">
                <div class="btn cancel">Cancel</div>
            </div>
        </form>
    </div>
</div>