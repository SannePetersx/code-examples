// Inject socket.io into the header based on the current url
var host = function ()
{
    return '//' + window.location.host + ':' + ((window.location.href.indexOf('staging') == -1) ? '8080' : '8081');
};

//================================================================================
// General functions, used all over the application
//================================================================================

/**
 * Make task draggable.
 */
function makeTaskDraggable()
{
    $('.task').draggable({
        containment: ".draggable",
        cursor: "move",
        snap: ".column",
        distance: 20
    });
}

/**
 * Detach and append task to column.
 * @param elm
 * @param column
 */
function appendToColumn(elm, column)
{
    $(elm).hide();
    $(elm).detach().css({
        top: 0,
        left: 0
    }).appendTo(column).fadeIn(500);
}

/**
 * Toggle extra info on task.
 */
$(function ()
{
    $(document).on('click', '.showOrHide', function ()
    {
        if ($(this).siblings('.data').is(":visible")) {
            $(this).siblings('.data').hide();
            $(this).parent().find('.showOrHide').html('more <i class="fa fa-angle-down" aria-hidden="true"></i>');
        } else {
            $(this).siblings('.data').show();
            $(this).parent().find('.showOrHide').html('less <i class="fa fa-angle-up" aria-hidden="true"></i>');
        }
    });
});

/**
 * If column has a subtext, place task above it.
 */
function placeAboveSubtext(elm)
{
    if (elm.children().length > 0) {
        var columnSubText = elm.children('.columnSubtext');
        columnSubText.remove();
        elm.append(columnSubText);
    }
}

/**
 * Returns the size of the given object
 * @param obj
 * @returns {number}
 */
function objectSize(obj)
{
    var size = 0,
        key = 0;

    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            size++;
        }
    }
    return size;
}

/**
 * Get monday of the current week
 * @param date
 * @returns {date}
 */
function getMonday(date)
{
    var day = date.getDay();

    if (day !== 1) {
        date.setHours(-24 * (day - 1));
    }

    var displayDate = date.getDate() + " " + getMonthName(date.getMonth());
    $('.firstDayOfThisWeek').html(displayDate);
    return date;
}

/**
 * Get the name of the month by the given month
 *
 * @param month
 * @returns {string}
 */
function getMonthName(month)
{
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    return monthNames[month];
}

/**
 * Close error message
 */
$(function ()
{
    $(document).on('click', '.closeError', function ()
    {
        $(this).parent().remove();
    });
});

var filter, selectedOption, filters = {
    employee: 'all',
    company: 'all',
    project: 'all',
    epic: 'all',
    ticket: 'all'
};

/**
 * Edit input field for starting date in done column
 */
$(function ()
{
    $(document).on('focusout', '.done_since', function ()
    {
        var beginDoneSince = $('.done_since').val(),
            date = new Date(beginDoneSince),
            loadingIcon = $('.loadingIcon'),
            errorsDiv = $('.errors');

        loadingIcon.show();
        $('.column[data-status="done"] .task').remove();

        $('.firstDayOfThisWeek').html(date.getDate() + ' ' + getMonthName(date.getMonth()));

        $.ajax({
            url: '/planboard/gettasksfromdate',
            method: 'POST',
            dataType: 'JSON',
            data: {
                filters: filters,
                date: beginDoneSince
            },
            success: function (data)
            {
                errorsDiv.empty();

                $('div[data-status="done"] .task').remove();

                $('.hideEditFirstDay').trigger('click');

                if (data.length < 100) {
                    $('.errors').html('<div class="error">There are no done tasks found from this date <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                    loadingIcon.hide();
                    return;
                }

                $.each(data, function (key, value)
                {
                    var statusColumn = $('.column[data-status="done"]');
                    statusColumn.append(value.template).fadeIn(500);
                    placeAboveSubtext(statusColumn);
                    makeTaskDraggable();
                });
                loadingIcon.hide();
            },
            error: function (data)
            {
                console.log('Ajax not working because of: ' + data.responseText);
                errorsDiv.html('<div class="error">The filter data could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                loadingIcon.hide();
            }
        });
    });
});

/**
 * Show edit input field for starting date in done column
 */
$(function ()
{
    $(document).on('click', '.showEditFirstDay', function ()
    {
        $('.currentDay').hide();
        $('.editFirstDay').show();
    });
});

/**
 * Hide edit input field for starting date in done column
 */
$(function ()
{
    $(document).on('click', '.hideEditFirstDay', function ()
    {
        $('.editFirstDay').hide();
        $('.currentDay').show();
    });
});

/**
 * Filter tasks
 */
$(function ()
{
    $('.filter').change(function (event, filter_option, filter_value)
    {
        filter = $(this).data('filter');
        selectedOption = $(this).val();
        filters[filter] = $(this).val();

        $('.loadingIcon').show();

        $.ajax({
            url: '/planboard/filter',
            method: 'POST',
            dataType: 'JSON',
            data: {
                filters: filters
            },
            success: function (data)
            {
                $('.errors').empty();
                $('.task').remove();

                $.each(data, function (key, value)
                {
                    var statusColumn = $('.column[data-status="' + value.status + '"]');
                    statusColumn.append(value.template).fadeIn(500);
                    placeAboveSubtext(statusColumn);
                    makeTaskDraggable();
                });

                $('.loadingIcon').hide();
            },
            error: function (data)
            {
                console.log('Ajax not working because of: ' + data.responseText);
                $('.errors').html('<div class="error">The filter data could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');

                $('.loadingIcon').hide();
            }
        });

        var monday = getMonday(new Date());
        var day = ("0" + monday.getDate()).slice(-2);
        var month = ("0" + (monday.getMonth() + 1)).slice(-2);

        var date = monday.getFullYear() + "-" + (month) + "-" + (day);

        $('[name="done_since"]').val(date);
        $('.firstDayOfThisWeek').html(monday.getDate() + ' ' + getMonthName(monday.getMonth()));
    });
});

/**
 * Clear filter
 */
$(function ()
{
    $('.clearFilter').click(function ()
    {
        var form = $('#filterForm');
        form[0].reset();

        for (var j in filters) {
            filters[j] = 'all';
        }

        // Set all filter values to 'all' for select2.
        $('#filterForm select').val('all').trigger('change.select2');

        // Trigger change event one time to get all tasks.
        $('#filterForm select[data-filter="employee"]').trigger('change', ['default', 'all']);
        $('.task').remove();
    });
});

/**
 * Init planboard
 */
$(function ()
{
    /**
     * Add monday from this week to columnSubtext.
     */
    getMonday(new Date());

    /**
     * Add select2 functions to .select2 selectboxes.
     */
    $('.select2').select2();

    /**
     * Add datepicker for safari, because date field is not supported.
     */
    var dateField = $('[type="date"]');
    if (dateField.prop('type') != 'date') {
        dateField.datepicker();
    }

    /**
     * Disable drag for tasks with status done on load.
     */
    $(function ()
    {
        $('[data-status="done"] .task').draggable('disable');
    });

    makeTaskDraggable();
});

//================================================================================
// Socket functions
//================================================================================
try {
    var socket = io.connect(host());
    socket.on('message', function (data)
    {
        $('.errors').empty();

        var decode = JSON.parse(data),
            statusColumn = $('.column[data-status="' + data.status + '"]');

        // Add task
        if (decode.action == "add") {

            // Load the new task
            $.ajax({
                url: '/planboard/loadtask/' + decode.taskID,
                method: 'POST',
                dataType: 'JSON',
                data: {},
                success: function (data)
                {
                    statusColumn = $('.column[data-status="' + data.status + '"]');
                    statusColumn.append(data.template).fadeIn(500);
                    placeAboveSubtext(statusColumn);
                },
                error: function (data)
                {
                    console.log('Ajax not working because of: ' + data.responseText);
                    $('.errors').append('<div class="error">The new task could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                }
            });
        }

        // Edit task
        if (decode.action == "edit") {
            $('.task[data-id="' + decode.taskID + '"]').remove();

            /**
             * Load the updated task.
             */
            console.log('load');
            $.ajax({
                url: '/planboard/loadtask/' + decode.taskID,
                method: 'POST',
                dataType: 'JSON',
                data: {
                    task_id: decode.taskID
                },
                success: function (data)
                {
                    statusColumn = $('.column[data-status="' + data.status + '"]');
                    statusColumn.append(data.template).fadeIn(500);
                    placeAboveSubtext(statusColumn);
                },
                error: function (data)
                {
                    console.log('Ajax not working because of: ' + data.responseText);
                    $('.errors').append('<div class="error">The updated task could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                }
            });
        }

        // Move task
        if (decode.action == "move") {
            appendToColumn('.task[data-id="' + decode.taskID + '"]', $('.column[data-status="' + decode.status + '"]'));
        }

        // Delete task
        if (decode.action == "delete") {
            $('.task[data-id="' + decode.taskID + '"]').remove();
        }
    });
} catch (e) {
    console.log('Sorry, but there is a problem with your socket connection or the server is down: ' + e.message);
    $(function ()
    {
        $('.errors').html('<div class="error">There is a problem with your socket connection or the server is down. Please send an e-mail to <a href="mailto:support@webiq.nl">support@webiq.nl</a></div>');
        $('.columnWrapper').empty();
    });
}

//================================================================================
// Modal functions
//================================================================================

$(function ()
{
    /**
     * Show addTask modal
     */
    $(document).on('click', '.addTask', function ()
    {
        $('.errors').empty();

        $.ajax({
            url: '/planboard/addform',
            method: 'POST',
            dataType: 'JSON',
            data: {},
            success: function (data)
            {
                successAjax('add', data);
            },
            error: function (data)
            {
                console.log('Ajax not working because of: ' + data.responseText);
                $('.errors').append('<div class="error">The add form could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                recalculateWrapperHeight();
            }
        });
    });

    /**
     * Show editTask modal
     */
    $(document).on('click', '.editTask', function ()
    {
        $('.errors').empty();

        $.ajax({
            url: '/planboard/editform',
            method: 'POST',
            dataType: 'JSON',
            data: {
                id: $(this).data('id')
            },
            success: function (data)
            {
                successAjax('edit', data);
            },
            error: function (data)
            {
                console.log('Ajax not working because of: ' + data.responseText);
                $('.errors').append('<div class="error">The edit form could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                recalculateWrapperHeight();
            }
        });
    });

    /**
     * Show deleteTask modal
     */
    $(document).on('click', '.deleteTask', function ()
    {
        $('.errors').empty();

        $.ajax({
            url: '/planboard/deleteform',
            method: 'POST',
            dataType: 'JSON',
            data: {
                id: $(this).data('id')
            },
            success: function (data)
            {
                successAjax('delete', data);
            },
            error: function (data)
            {
                console.log('Ajax not working because of: ' + data.responseText);
                $('.errors').append('<div class="error">The delete form could not be loaded. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                recalculateWrapperHeight();
            }
        });
    });

    /**
     * Return which modal is open
     */
    function whichModal()
    {
        if ($('.editTaskModal').css("display") == "block") {
            return 'edit';
        } else if ($('.addTaskModal').css("display") == "block") {
            return 'add';
        } else if ($('.deleteTaskModal').css("display") == "block") {
            return 'delete';
        }
    }

    /**
     * Make task droppable
     */
    $('.column').droppable({
        drop: function (event, ui)
        {
            var current = $(this),
                newStatus = current.data('status'),
                oldStatus = ui.draggable.data('status'),
                taskID = ui.draggable.data('id'),
                statusColumn = $('.column[data-status="' + oldStatus + '"]'),
                newStatusColumn = $('.column[data-status="' + newStatus + '"]');

            /**
             * Change task status
             */
            $('.errors').empty();

            if (oldStatus != newStatus) {
                $.ajax({
                    url: '/planboard/edittaskstatus',
                    method: 'POST',
                    data: {
                        task_id: taskID,
                        status: newStatus
                    },
                    success: function (data)
                    {
                        if (data == true) {
                            appendToColumn(ui.draggable, newStatusColumn);
                            $(ui.draggable).attr('data-status', newStatus);
                            $(ui.draggable).data('status', newStatus);
                            placeAboveSubtext(newStatusColumn);

                            if (newStatus == 'done') {
                                ui.draggable.draggable('disable');
                            }

                            socket.send('{"action":"move", "taskID":' + taskID + ',"status":"' + newStatus + '"}');

                            $('.errors').empty();
                        } else {
                            $('.errors').html('<div class="error">' + data + '<i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                            appendToColumn(ui.draggable, statusColumn);
                            placeAboveSubtext(statusColumn);
                            recalculateWrapperHeight();
                        }
                    },
                    error: function (data)
                    {
                        console.log('Ajax not working because of: ' + data.responseText);
                        $('.errors').html('<div class="error">The status could not be changed. Please contact support.<i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                        appendToColumn(ui.draggable, statusColumn);
                        placeAboveSubtext(statusColumn);
                        recalculateWrapperHeight();
                    }
                });
            } else {
                appendToColumn(ui.draggable, statusColumn);
                placeAboveSubtext(statusColumn);
            }
        }
    });
});

/**
 * Do actions when succes on Ajax calls for modals
 * @param modal
 * @param data
 */
function successAjax(modal, data)
{
    var modalContainer = $('.modalContainer');
    modalContainer.append(data).fadeIn(200);
    $('.select2').select2();

    /**
     * Save modal data.
     */
    $('.save').on('click', function ()
    {
        var current = $(this),
            taskID = current.data('id'),
            status = 'backlog',
            statusColumn = $('.column[data-status="' + status + '"]');

        if (modal == 'add') {
            /**
             * Add modal function
             */
            if ($('.addTaskForm')[0].checkValidity()) {
                $('.modalErrors').empty();

                $.ajax({
                    url: '/planboard/add',
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        project_id: $('[name="project_id"]').val(),
                        epic_id: $('[name="epic_id"]').val(),
                        assignee: $('[name="assignee"]').val(),
                        difficulty: $('[name="difficulty"]').val(),
                        title: $('[name="title"]').val(),
                        billable: $('[name="billable"]').prop('checked'),
                        description: $('[name="description"]').val(),
                        due_date: $('[name="due_date"]').val(),
                        estimated_time: $('[name="estimated_time"]').val()
                    },
                    success: function (data)
                    {
                        if (data.template.length > 400) {
                            var newStatusColumn = $('.column[data-status="' + data.status + '"]');
                            newStatusColumn.append(data.template).fadeIn(500);
                            placeAboveSubtext(newStatusColumn);
                            hideModal();
                            makeTaskDraggable();
                            socket.send('{"action":"add", "taskID":' + data.task_id + ',"status":"' + data.status + '"}');
                        } else {
                            $('.modalErrors').html('<div class="error">' + data + '</div>');
                        }
                    },
                    error: function (data)
                    {
                        console.log(data);
                        $('.modalErrors').html('<div class="error">' + data.responseText + '</div>');
                    }
                });
            } else {
                $('.modalErrors').html('<div class="error">Not all required fields are filled in.</div>');
            }
        } else if (modal == 'edit') {
            /**
             * Edit modal function
             */

            $.ajax({
                url: '/planboard/edit/' + taskID,
                method: 'POST',
                dataType: 'JSON',
                data: {
                    assignee: $('[name="assignee"]').val(),
                    billable: $('[name="billable"]').prop('checked'),
                    description: $('[name="description"]').val(),
                    due_date: $('[name="due_date"]').val(),
                    difficulty: $('[name="difficulty"]').val(),
                    estimated_time: $('[name="estimated_time"]').val(),
                    time_spend: $('[name="time_spend"]').val()
                },
                success: function (data)
                {
                    $('.task[data-id="' + taskID + '"]').remove();
                    statusColumn = $('.column[data-status="' + data.status + '"]');
                    statusColumn.append(data.template).fadeIn(200);
                    placeAboveSubtext(statusColumn);
                    hideModal();
                    makeTaskDraggable();
                    socket.send('{"action":"edit", "taskID":' + data.task_id + ',"status":"' + data.status + '"}');
                },
                error: function (data)
                {
                    console.log('Ajax not working');
                    $('.modalErrors').html('<div class="error">' + data.responseText + '</div>');
                }
            });
            /**
             * Delete modal function
             */
        } else if (modal == 'delete') {
            $.ajax({
                url: '/planboard/deletetask',
                method: 'POST',
                data: {
                    task_id: taskID
                },
                success: function (data)
                {
                    if (data == true) {
                        socket.send('{"action":"delete", "taskID":' + taskID + '}');
                        $('.task[data-id="' + taskID + '"]').remove();
                        hideModal();
                        $('.errors').empty();
                    } else {
                        $('.errors').html('<div class="error">' + data.responseText + '<i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                    }
                },
                error: function (data)
                {
                    console.log('Ajax not working because of: ' + data.responseText);
                    $('.errors').html('<div class="error">The task could not be deleted because ajax is not working. <i class="fa fa-times closeError" aria-hidden="true"></i></div>');
                }
            });
        }
    });

    /**
     * Add datepicker for safari, because date field is not supported.
     */
    var dateField = $('[type="date"]');
    if (dateField.prop('type') != 'date') {
        dateField.datepicker();
    }

    /**
     * Modal: Click to hide
     */
    $('.hideModal, .cancel, .overlay').click(function ()
    {
        hideModal();
    });

    /**
     * Modal: Prevent hiding if you click inside it
     */
    $('.modal').click(function (e)
    {
        e.stopPropagation();
    });

    /**
     * Hide modal.
     */
    function hideModal()
    {
        modalContainer.fadeOut(500, function ()
        {
            $(this).empty();
        });
    }

    /**
     * Modal: Show and hide fields depending on what assignee is selected.
     */
    var fieldsOnlyYouMaySee = $('.onlyShowIfAssigneeIsYou'),
        selectBoxEmployee = $('#employeeSelector');

    /**
     * Modal: Show fields if an employee is selected.
     */
    selectBoxEmployee.change(function ()
    {
        if (checkIfSelectedEmployeeIsYou()) {
            fieldsOnlyYouMaySee.show();
        } else {
            fieldsOnlyYouMaySee.hide();
            $('[name="difficulty"]').val('');
            $('[name="estimated_time"]').val('');
        }
    });

    // On load of modal.
    if (modal) {
        if (selectBoxEmployee.val()) {
            if (checkIfSelectedEmployeeIsYou()) {
                fieldsOnlyYouMaySee.show();
            } else {
                fieldsOnlyYouMaySee.hide();
                $('[name="difficulty"]').val('');
                $('[name="estimated_time"]').val('');
            }
        }
    }

    /**
     * Modal: Check if employee selected is also the employee that is logged in.
     */
    function checkIfSelectedEmployeeIsYou()
    {
        return (fieldsOnlyYouMaySee.data('id') == selectBoxEmployee.val());
    }
}

/**
 * Modal: Get epics from project when a project is selected.
 * @param field
 */
function getEpics(field)
{
    if ($(field).val() != '') {
        var projectID = $(field).val();

        $.ajax({
            url: '/planboard/getepics',
            method: 'POST',
            data: {
                id: projectID
            }
        }).done(function (data)
        {
            createEpicsField($.parseJSON(data));
            $('.errors').empty();
        }).error(function ()
        {
            console.log('Ajax not working because of: ' + data.responseText);
            $('.modalErrors').html('<div class="error">We could not get the epics of the project.</div>');
        });
    } else {
        $('#epics').html('<span class="text">Select a project first.</span>');
    }
}

/**
 * Modal: Create epics field
 * @param epics
 */
function createEpicsField(epics)
{
    var epicsField = $('#epics'),
        x = 0;

    epicsField.html('');

    if (epics != false) {
        // If there is more then one epic
        if (objectSize(epics) > 1) {
            var selectArrow = $('<span class="selectArrows"></span>'),
                select = $('<select name="epic_id" class="selectBoxEpic select2" required></select>');

            for (x in epics) {
                select.append('<option value=' + x + '>' + epics[x] + '</option>');
            }
            selectArrow.append(select);
            epicsField.append(selectArrow);
            // If there is only one epic
        } else {
            for (x in epics) {
                epicsField.append('<input type=\'hidden\' name=\'epic_id\' value=\'' + x + '\' />').append('<span class="text singleEpic">' + epics[x] + '</span>');
            }
        }
    } else {
        epicsField.append('<span class="text noEpic">No epics found, select a different project or <a href="/project/' + $('[name="project_id"]').val() + '" class="addEpic">add epic to project</a>.</span>');
    }
}
