<!DOCTYPE html>
<!--
     *****************************************************************
		Someone is curious. Feel free to take a look at the code :]
	   ************************************************************
-->
<html lang="en">
<head>
    <title>Sanne Peters | Front-End Developer & Designer</title>

    <meta charset="UTF-8">
    <meta name="description"
          content="A website designer &amp; developer looking to help bring your ideas to reality.">
    <meta name="keywords"
          content="Web Design, Wordpress, website design, gelderland, arnhem, nederland, cheap website, freelancer, html, css, jquery, responsive design, ux, Portfolio, Web Development, Photoshop, Design, PHP, Javascript, Multimedia Design, CMD, Graphic Design, Informatica">
    <meta property="og:image" content="http://www.sannepeters.nl/img/websitePreview.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon">

    <!--    Google Analytics-->
    <script>
        (function (i, s, o, g, r, a, m)
        {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function ()
                {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-38180564-2', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div class="container">
