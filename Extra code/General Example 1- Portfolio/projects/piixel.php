<?php include('../header.php') ?>
<?php include('../nav.php') ?>

<a href="/">
    <div class="logo"></div>
</a>

<div class="project">
    <h1>Piixel |  WordPress Blog</h1>

    <h2>Overview</h2>

    <p>
        Piixel is a blog made with WordPress. I started by making a Wordpress theme from scratch. This is a personal
        project because I liked blogging and wanted to learn Wordpress.
        I made sure that Piixel is easy to find on Google by applying SEO, and I often ask users for their opinion to
        improve the User Experience. Piixel currently has 5000 pageviews a month. I started blogging on Piixel in 2014.
    </p>

    <a class="url" href='http://www.piixel.nl' target='_blank' title='Piixel.nl'>www.Piixel.nl</a>

    <img src="/img/projects/piixel/details/desktop.png" alt="Piixel"><br><br>

    <h2>Research</h2>

    <p>To grow my blog bigger I did a lot of research on other blogs, SEO and social media sharing. By doing this I
        reached 1263 followers on Bloglovin'.</p><br><br>

    <img src="/img/projects/piixel/details/desktop2.png" alt="Piixel"><br><br>

    <h2>Design</h2>

    <p>The design has changed a lot through the years because I felt like the way you display a post determines a big
        part of how easy and catchy it is to read. I started out with wireframes and created a design with those in
        Photoshop.</p>

<!--    <img src="/img/projects/piixel/details/mobile.png" alt="Piixel"><br><br>-->

    <h2>Final thoughts</h2>

    <p>Even though Piixel was a personal project it teached me a lot about Wordpress, SEO, Blogging, Design & Affiliate
        marketing.</p><br><br>

</div>
</div>

<script type="text/javascript" src="/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
</body>
</html>