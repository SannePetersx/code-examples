<?php include('../header-nofollow.php') ?>
<?php include('../nav.php') ?>

<a href="/">
    <div class="logo"></div>
</a>

<div class="project">
    <h1>Support System | Web Application</h1>

    <h2>Overview</h2>

    <p>
        The support system is a web application that I created at my internship at <a href='http://www.webiq.nl' target='_blank'
                                                                                      title='WebIQ.nl'>WebIQ</a>.
        I created it in a team together with a designer and back-end developer where I was the front-end developer.
        It is an all around tool that includes a customer relation management system and a project management tool for
        planning tasks.
    </p>

    <img src="/img/projects/supportsystem/details/crm.png" alt="Supportsystem"><br>

    <p>The application was made with: HTML5, CSS3, Smarty (PHP templating engine), jQuery, PHP and Domain-driven
        design.</p>

    <img src="/img/projects/supportsystem/details/planboard.png" alt="Supportsystem"><br>

    <p>The general system is created in the team of 3 and the planboard was my own little project using sockets for live
        reloading.
        The planboard makes use of the back-end of the general system, and the design and front-end were created by me.</p>

    <img src="/img/projects/supportsystem/details/planboard-addtask.png" alt="Supportsystem"><br><br>

    <h2>Preview</h2><br>

    <div class="youtube">
        <iframe src="https://www.youtube.com/embed/SmJqE90nDR8?rel=0" frameborder="0" gesture="media"
                allowfullscreen class="video"></iframe>
    </div>
    <br><br>
</div>

<script type="text/javascript" src="/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
</body>
</html>