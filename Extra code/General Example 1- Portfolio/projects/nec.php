<?php include('../header.php') ?>
<?php include('../nav.php') ?>

<a href="/">
    <div class="logo"></div>
</a>

<div class="project">
    <h1>N.E.C. | Hybrid mobile application</h1>

    <h2>Overview</h2>

    <p>
        To finish my studies I did an internship at <a href='http://www.linku.nl' target='_blank'
                                                     title='Linku.nl'>LinkU</a>.
        During my internship I created a hybrid mobile application for N.E.C. Nijmegen, a soccer club.
        This application can run as a native Android and iOS app and can also run in the browser.
    </p>

    <img src="/img/projects/nec/details/mobile.png" alt="Piixel"><br><br>

    <p>
        The application is made with Ionic 3 and Angular 4. It uses static data from WordPress and live data
        through Firebase.
    </p>

    <img src="/img/projects/nec/details/mobile2.png" alt="Piixel"><br><br>

    <p>
        This was a solo project apart from the designs, which were created by a designer from <a
                href='http://www.linku.nl' target='_blank' title='Linku.nl'>LinkU</a>.
    </p>

    <h2>Preview</h2>

    <div class="youtube">
        <iframe src="https://www.youtube.com/embed/TFP0a-0FjzQ?rel=0" frameborder="0" gesture="media"
                allowfullscreen class="video"></iframe>
    </div>
    <br><br>
</div>
</div>

<script type="text/javascript" src="/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
</body>
</html>