<?php include('../header.php') ?>
<?php include('../nav.php') ?>

<a href="/">
    <div class="logo"></div>
</a>

<div class="project">
    <h1>Wijchen Verenigd | Online Platform</h1>

    <img src="/img/projects/wijchenverenigd/details/logo.png" alt="Wijchen Verenigd">

    <h2>Overview</h2>

    <p>For a school project we created a social platform to make people more active in Wijchen a city in the Netherlands.
        We decided to make a platform where people can find a buddy to work out with.
        I created this together with 5 other students where I did
        the programming together with 3 others, we developed the platform in Node.JS with MongoDB as the database.
    </p><br>

    <img src="/img/projects/wijchenverenigd/details/desktop1.png" alt="Wijchen Verenigd"><br><br>

    <h2>Research</h2>

    <p>To find out what people in Wijchen liked when it came to working out, we did a couple interviews. We also figured
        out that they have a lot of sport associations in Wijchen. Because of that we decided to create a platform where
        users could find others to work out with. Sport associations would also be able to place information about themselves to recruit new members.</p><br><br>

    <img src="/img/projects/wijchenverenigd/details/desktop2.png" alt="Wijchen Verenigd"><br><br>

    <h2>Design</h2>

    <p>
        The design was created with usability on the first place. We did a lot of user testing to make sure the platform
        was as usable as it could be.
    </p><br><br>

    <img src="/img/projects/wijchenverenigd/details/desktop3.png" alt="Wijchen Verenigd"><br><br>

    <h2>Final thoughts</h2>

    <p>
        This project taught me a bit about Node.JS and social platforms. Because this was only a concept with a Node.JS
        prototype it isn't available online.
    </p><br><br>
</div>

<script type="text/javascript" src="/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
</body>
</html>