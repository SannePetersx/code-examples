<?php include 'header.php' ?>
<div class="cv">
    <h1><span>Sanne Peters</span></h1>
    <a href="/" class="back">
        <i class="fa fa-angle-left 3x" aria-hidden="true"></i> <span>Back</span></a>
    <div class="container">
        <div class="info">
            <ul>
                <li>Front-end Developer</li>
                <li>User Experience</li>
                <li>Perfectionist</li>
                <li>Leergierig</li>
            </ul>
            <div class="user"></div>
            <ul>
                <li><a href="http://www.sannepeters.nl">SannePeters.nl | Portfolio</a></li>
                <li><a href="mailto:sanne@sannepeters.nl?Subject=CV">Sanne@SannePeters.nl</a></li>
                <li>17 December 1989</li>
                <li>Arnhem, Gelderland</li>
            </ul>
        </div>
        <h2>Education</h2>
        <div class="past">
            <div class="table">
                <div class="row">
                    <div class="cell"><a href="http://www.han.nl" target="_blank">Hogeschool Arnhem Nijmegen</a> | HBO
                        <span class="certificates">Informatica - Communication &amp; Multimedia Design | Diploma behaald</span>
                    </div>
                    <div class="cell date">2013
                        <span class="month">aug</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2017
                        <span class="month">nov</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.hu.nl" target="_blank">Hogeschool Utrecht</a> | HBO
                        <span class="certificates">Communication &amp; Multimedia Design | Propedeuse behaald</span>
                    </div>
                    <div class="cell date">2011
                        <span class="month">aug</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2012
                        <span class="month">juni</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.graafschapcollege.nl" target="_blank">Graafschap College</a> |
                        MBO
                        <span class="certificates">Niveau 3 ICT Beheer | Diploma behaald</span>
                        <span class="certificates">Niveau 4 Applicatie ontwikkeling | Diploma behaald</span>
                    </div>
                    <div class="cell date">2006
                        <span class="month">aug</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2010
                        <span class="month">juni</span>
                    </div>
                </div>
            </div>
        </div>
        <h2>Experience</h2>
        <div class="past">
            <div class="table">
                <div class="row">
                    <div class="cell"><a href="http://www.linku.nl" target="_blank">LinkU</a> | Werkervaring
                        <span class="certificates">Hybride mobiele applicatie realiseren voor N.E.C. Nijmegen (Ionic 3, Angular 4 &amp; Firebase)</span>
                    </div>
                    <div class="cell date">2017
                        <span class="month">apr</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2017
                        <span class="month">nov</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.han.nl" target="_blank">Hogeschool Arnhem Nijmegen</a> |
                        Studentassistent Webdevelopment
                        <span
                                class="certificates">CMD Studenten ondersteunen bij het maken van een website (HTML &amp; CSS)</span>
                    </div>
                    <div class="cell date">2016
                        <span class="month">okt</span>
                    </div>

                    <div class="cell dash"> -</div>
                    <div class="cell date">2017
                        <span class="month">jan</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.webiq.nl" target="_blank">WebIQ</a> | Werkervaring
                        <span
                                class="certificates">Supportsysteem webapplicatie realiseren (HTML, CSS, jQuery &amp; Smarty)</span>
                    </div>
                    <div class="cell date">2015
                        <span class="month">feb</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2015
                        <span class="month">juni</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.youshiki.nl" target="_blank">Youshiki</a> | Werkervaring
                        <span class="certificates">Ontwerpen en uitwerken van websites (HTML, CSS  &amp; PHP)</span>
                    </div>
                    <div class="cell date">2009
                        <span class="month">sep</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2010
                        <span class="month">feb</span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell"><a href="http://www.attent.nl" target="_blank">Attent WWZ</a> | Keukenmedewerkster
                        <span class="certificates">Maaltijden samenvoegen en afwassen in een verzorgingstehuis</span>
                    </div>
                    <div class="cell date">2004
                        <span class="month">nov</span>
                    </div>
                    <div class="cell dash"> -</div>
                    <div class="cell date">2011
                        <span class="month">mei</span>
                    </div>
                </div>
            </div>
        </div>
        <h2>Skills</h2>
        <div class="skillset">
            <div class="table">
                <div class="row">
                    <div class="cell">HTML &amp; CSS (SASS)</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                    </div>
                    <div class="cell">Adobe Photoshop</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">JavaScript &amp; jQuery</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                    </div>
                    <div class="cell">User Experience</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">Angular 4</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                    </div>

                    <div class="cell">WordPress</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="cell">Ionic 3</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                    </div>
                    <div class="cell">PHP</div>
                    <div class="cell rating">
                        <span class="circle filled"></span>
                        <span class="circle filled"></span>
                        <span class="circle"></span>
                        <span class="circle"></span>
                        <span class="circle"></span>
                    </div>
                </div>
            </div>
        </div>
            <h2>Other skills</h2>
            <div class="skillset">
                <div class="table">
                    <div class="row">
                        <div class="cell">GIT</div>
                        <div class="cell rating">
                        </div>
                        <div class="cell">Wireframing</div>
                        <div class="cell rating">
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">Responsive Design</div>
                        <div class="cell rating">
                        </div>
                        <div class="cell">SCRUM</div>
                        <div class="cell rating">
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">Model View Controller</div>
                        <div class="cell rating">
                        </div>
                        <div class="cell">SEO</div>
                        <div class="cell rating">
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">MySQL</div>
                        <div class="cell rating">
                        </div>
<!--                        <div class="cell">SEO</div>-->
<!--                        <div class="cell rating">-->
<!--                        </div>-->
                    </div>
                </div>
         </div>
        </div>
    </div>
</div>
</div>
</body>
</html>