<?php
if ($_POST["submit"]) {
    echo sendEmail($_POST['name'], $_POST['email'], 'E-Mail van Sannepeters.nl', $_POST['message'], 'sannepetersx@gmail.com');
} else {
    echo '<p>Something went wrong, please send and e-mail to sanne@sannepeters.nl I will reply as soon as possible.</p>';
}

function sendEmail($fromName, $fromEmail, $subject, $message, $toEmail)
{
    if (!filter_var($fromEmail, FILTER_VALIDATE_EMAIL)) {
        return json_encode(array("type" => "error", "msg" => "Please enter a valid e-mail address."));
    }
    if (trim($fromName) == "") {
        return json_encode(array("type" => "error", "msg" => "Please enter your name."));
    }
    if (trim($message) == "") {
        return json_encode(array("type" => "error", "msg" => "Please enter a message."));
    }

    $emailMessage = "E-Mail van wwww.sannepeters.nl:<br><br>";
    $emailMessage .= "Naam: " . $fromName . "<br>";
    $emailMessage .= "E-mailadres: " . $fromEmail . "<br>";
    $emailMessage .= "Bericht: " . $message . "<br><br>";
    $emailMessage .= "IP: " . $_SERVER['REMOTE_ADDR'];

    // To send HTML mail, the Content-type header must be set
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
    $headers .= "Reply-to: $fromEmail";

    mail($toEmail, $subject, $emailMessage, $headers);

    return json_encode(array("type" => "success", "msg" => 'Thanks for your message. I will reply as soon as possible.'));
}

?>