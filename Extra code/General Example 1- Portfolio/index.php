<?php include 'header.php' ?>
<?php include 'nav.php' ?>

<a href="/">
    <div class="logo"></div>
</a>

<div class="block intro">
    <div class="insideBlock">
        <h1 id="home">Front-End Developer</h1>

        <p>80% Programming | 10% Design | 10% User Experience</p>
    </div>
</div>
<div class="block workSlider">
    <div class="inner">
        <div class="left">
        </div>
        <div class="right">
        </div>
    </div>
    <div class="selectors">
    </div>
</div>
<div class="block about">
    <div class="insideBlock about">
        <h2 id="about">Sanne Peters</h2>

        <div class="contentLeft">

            <div class="description">
                <img src="./img/sannepeters.jpg" alt="Sanne Peters">

                <p class="firstP">I'm a Front-End Developer & Designer located in Arnhem, the Netherlands. I developed a
                    passion for webdevelopment when I was 12 years old. </p>
                <p>I specialize in front-end development for <strong>websites</strong>, <strong>webapplications</strong>
                    and <strong>mobile apps</strong>.

                <p>I'm almost done with my studies and open for job offers, feel free to send me a <a href="#contact">message</a>.
                </p>
            </div>

            <div class="social">
                <div class="flexBox">
                    <div class="flexRow">
                        <div class="flexChild">
                            <a href="/cv.php" target="_blank" title="View my resume"><i
                                        class="fa fa-file-text"></i> Resume (Dutch)</a></div>
                        <div class="flexChild"><a href="https://www.facebook.com/SannePetersNL" target="_blank"
                                                  title="Facebook"> <i
                                        class="fa fa-facebook-square"></i> Facebook
                            </a></div>
                        <div class="flexChild"><a href="http://nl.linkedin.com/in/SannePetersNL" target="_blank"
                                                  title="LinkedIn">
                                <i class="fa fa-linkedin-square"></i> LinkedIn
                            </a></div>
                        <div class="flexChild"><a href="https://github.com/SannePetersNL" target="_blank"
                                                  title="GitHub">
                                <i class="fa fa-github-square"></i> GitHub
                            </a></div>
                    </div>
                </div>
            </div>

            <h3 id="otherExperience" class="otherExperienceBtn">Other experience <i class="fa fa-caret-down"
                                                                                    aria-hidden="true"></i></h3>

            <div class="otherExperience" style="display:none;">
                <div class="left">
                    <ul>
                        <li>Git</li>
                        <li>Responsive Design</li>
                        <li>SCRUM</li>
                        <li>Wireframing</li>
                    </ul>
                </div>
                <div class="right">
                    <ul>
                        <li>PHP</li>
                        <li>MySQL (Databases)</li>
                        <li>Search Engine Optimisation (SEO)</li>
                        <li>Model View Controller</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="contentRight">
            <div class="bars-container">
                <p>HTML <span class="amp">&amp;</span> CSS (SASS/SCSS)</p>

                <div class="progress">
                    <span class="bar" style="width: 100%;"><span>95%</span></span>
                </div>

                <p>JavaScript & jQuery</p>

                <div class="progress">
                    <span class="bar" style="width: 80%;"><span>80%</span></span>
                </div>

                <p>Angular 4</p>

                <div class="progress">
                    <span class="bar" style="width: 80%;"><span>80%</span></span>
                </div>

                <p>Ionic 3</p>

                <div class="progress">
                    <span class="bar" style="width: 90%;"><span>80%</span></span>
                </div>

                <p>Adobe Photoshop</p>

                <div class="progress">
                    <span class="bar" style="width: 90%;"><span>90%</span></span>
                </div>

                <p>User Experience</p>

                <div class="progress">
                    <span class="bar" style="width: 80%;"><span>80%</span></span>
                </div>

                <p>WordPress</p>

                <div class="progress">
                    <span class="bar" style="width: 80%;"><span>80%</span></span>
                </div>

            </div>
            <div>

            </div>
        </div>
    </div>
</div>
<div class="block work" id="work">
    <div class="innerBlock">
        <h2>Work</h2>
        <div id="thumbnails"></div>
        <div class="preview" id="preview"></div>
    </div>
</div>
<div class="block contact">
    <div class="innerBlock">

        <h2 id="contact">Contact</h2>

        <div id="contactFormShow">
            <p>Interested in working together? Tell me a bit about your project and I'll get back to you!</p>

            <form method="post" action="sendMail.php" id="contactForm">
                <input type="text" name="name" placeholder="Name" id="name"
                       value="<?= (isset($_POST['name'])) ? $_POST['name'] : "" ?>" required>
                <input type="email" name="email" placeholder="E-mail" id="email"
                       value="<?= (isset($_POST['email'])) ? $_POST['email'] : "" ?>" required>
                <textarea name="message" placeholder="Your message"
                          id="message" required><?= $_POST['msgContent']; ?></textarea>

                <div class="submitBtnContainer">
                    <input type="submit" id="submit" name="submit" value="Send">
                </div>
            </form>
        </div>
        <div id="results"></div>
    </div>
    <div id="map"></div>

</div>
</div>

<script type="text/javascript" src="./js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="./js/vendor.js"></script>
<script type="text/javascript" src="./js/data.js"></script>
<script type="text/javascript" src="./js/main.js"></script>
<script type="text/javascript" src="./js/global.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyWGYuEJi4rARZbFlvkK-unxWuuVjmwbo&callback=initMap"
        async defer></script>
</body>
</html>
