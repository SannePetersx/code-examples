var imgDir = {
    main: './img/',
    projects: './img/projects/'
};

var currentSliderFrame;
var rotateSliderInterval = null;

/*
 * Slider.
 */
function showSlider() {
    var left = $('.workSlider .inner .left');
    var right = $('.workSlider .inner .right');

    left.append('<img src="' + imgDir.projects + sliderContents[0].filename + '/slider/slider.png" alt="' + sliderContents[0].name + '">');
    right.append('<h3>' + sliderContents[0].name + '</h3>');
    right.append('<p>' + sliderContents[0].description + '</p>');
    right.append('<a href="/projects/' + sliderContents[0].filename + '.php"><button id="viewBtn" data-id="' + sliderContents[0].id + '">View</button></a>');

    currentSliderFrame = 0;

    for (var i = 0; i < sliderContents.length; i++) {
        if (i == 0) {
            $('.selectors').append('<div class="selector activeSelector" id="' + sliderContents[i].id + '" data-id="' + sliderContents[i].id + '">');
        } else {
            $('.selectors').append('<div class="selector" id="' + sliderContents[i].id + '" data-id="' + sliderContents[i].id + '">');
        }
    }
}

$(document).on('click', '.selector', function () {
    var selectedSliderFrame = $(this).data('id');

    if (selectedSliderFrame != currentSliderFrame) {
        $('.activeSelector').removeClass('activeSelector');
        $('.selector[data-id=' + selectedSliderFrame + ']').addClass('activeSelector');

        showSliderFrame(selectedSliderFrame);

        currentSliderFrame = selectedSliderFrame;

        resetRotateSliderInterval();
    }
});

function showSliderFrame(frame){
    var left = $('.workSlider .inner .left');
    var right = $('.workSlider .inner .right');
    var inner = $('.workSlider .inner');

    inner.fadeOut(500, function () {

        left.empty().append('<img src="' + imgDir.projects + sliderContents[frame].filename + '/slider/slider.png" alt="' + sliderContents[frame].name + '" width="475">');
        right.empty().append('<h3>' + sliderContents[frame].name + '</h3>');
        right.append('<p>' + sliderContents[frame].description + '</p>');
        right.append('<a href="/projects/' + sliderContents[currentSliderFrame].filename + '.php"><button id="viewBtn" data-id="' + sliderContents[currentSliderFrame].id + '">View</button></a>');

        inner.fadeIn(500);
    });
}

/*
 * Rotate slider.
 */
function rotateSlider() {
    currentSliderFrame++;

    if (currentSliderFrame > sliderContents.length - 1) {
        currentSliderFrame = 0;
    }

    $('.activeSelector').removeClass('activeSelector');
    $('.selector[data-id=' + currentSliderFrame + ']').addClass('activeSelector');

    showSliderFrame(currentSliderFrame);

    selectedSliderID = currentSliderFrame;
}

function resetRotateSliderInterval() {
    clearInterval(rotateSliderInterval);
    rotateSliderInterval = setInterval(rotateSlider, 6000);
}

/*
 * Load work thumbnails.
 */
function showThumbnails() {
    for (var i = 0; i < sliderContents.length; i++) {
        $('#thumbnails').append('<a href="/projects/' + sliderContents[i].filename + '.php">' + '<div class="thumbnail"><div class="overlay"><p class="title">' + sliderContents[i].name + '</p><p>' + sliderContents[i].description + '</p></div>' + '<img src="' + imgDir.projects + sliderContents[i].filename + '/thumb/thumb.png" alt="' + sliderContents[i].name + '"></div></a>');
    }
}

/*
 * Show overlay on work thumbnails
 */
$(document).on({
    mouseenter: function () {
        $(this).find(".overlay").animate({opacity: 1}, 500);
    },
    mouseleave: function () {
        $(this).find(".overlay").animate({opacity: 0}, 200);
    }
}, ".thumbnail");

// Smooth scrolling nav
try {
    $('#nav').onePageNav({
        currentClass: 'current',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        filter: '',
        easing: 'swing'
    });
} catch (e) {
}

/*
 * Show otherExperience.
 */
$(function () {
    $('.otherExperienceBtn').click(function () {
        if ($('.otherExperience').is(':visible')) {
            $(this).html('Other experience <i class="fa fa-caret-down" aria-hidden="true"></i>');
        } else {
            $(this).html('Other experience <i class="fa fa-caret-up" aria-hidden="true"></i>');
        }
        $('.otherExperience').slideToggle('fast');
    });
});

/*
 * Send contact form e-mail.
 */
$(document).on('submit', '#contactForm', function (event) {
    event.preventDefault();

    var name = $('#name').val();
    var email = $('#email').val();
    var message = $('#message').val();
    var submit = $('#submit').val();


    $.post('sendMail.php', {email: email, name: name, message: message, submit: submit}, function (data) {
        if (data.type != 'error') {
            $("#contactFormShow").hide();
        }
        $('#results').html('<p>' + data.msg + '</p>');
    }, "JSON");
});

/**
 * Scroll functions
 */
$(function () {
    var triggeroffset = 300,
        classes = [],
        topoffset = 0,
        windowheight = 0,
        triggerd = [];

    // get block classes
    $(".block").each(function () {
        var offset = $(this).offset();
        var height = $(this).height();
        var classname = $(this).attr('class');
        classname = classname.replace('block', '');
        classname = classname.trim();
        if (classname != "") {
            classes.push({class: classname, top: offset.top, bottom: (height + offset.top)});
        }
    });

    $(document).scroll(function () {
        topoffset = $(document).scrollTop();
        windowheight = $(window).height();

        $(classes).each(function (key, block) {
            //Check if block is in viewport
            if ((block.top + triggeroffset) < (topoffset + windowheight)) {
                //Check if block is already triggerd
                if (triggerd.indexOf(block.class) == -1) {

                    //ANIMATIONS
                    if (block.class == 'work') {
                        $('.work .thumbnail').each(function (i, element) {
                            $(element).css({'opacity': 0});

                            setTimeout(function () {
                                $(element).animate({
                                    'opacity': 1.0
                                }, 450);
                            }, ( i * 500 ));

                        });
                    }

                    if (block.class == 'about') {
                        $(".about .progress").each(function (i, element) {
                            var maxwidth = parseInt($(element).find('.bar span').html().replace('%', ''));
                            $(element).find(".bar").css('width', '0%');
                            $(element).find(".bar span").html('0%');

                            setTimeout(function () {
                                var j = 0;
                                var currentinterval = setInterval(function () {
                                    j++;
                                    $(element).find(".bar").css('width', j + '%');
                                    $(element).find(".bar span").html(j + '%');

                                    if (j == maxwidth) {
                                        clearInterval(currentinterval);
                                    }
                                }, 2);
                            }, ( i * 300 ));

                        });
                    }
                    triggerd.push(block.class);
                }
            }
        });

    });
});

/*
 * Google maps.
 */
var map;

function initMap() {
    var position = {lat: 51.9815462, lng: 5.904103500000019};
    map = new google.maps.Map(document.getElementById('map'), {
        center: position,
        zoom: 13
    });
    var marker = new google.maps.Marker({
        position: position,
        map: map
    });
}

/*
 * Init
 */
$(function () {
    showSlider();
    resetRotateSliderInterval();
    showThumbnails();

    for(var i = 0; i < sliderContents.length; i++){
        $('<img>').attr('src', imgDir.projects + sliderContents[i].filename + '/slider/slider.png').appendTo('body').css('display', 'none');
    }

    try {
        google.maps.event.addDomListener(window, 'load', init_map);
    } catch (e) {

    }
});