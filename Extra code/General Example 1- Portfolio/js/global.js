/*
 * Hide and show menu.
 */
$(function () {
    var container = $('nav'),
        body = $('body'),
        open = 'menu-open';

    $('.hamburger').click(function () {
        toggleMenu();
    });

    $('nav > ul > li a').click(function () {
        toggleMenu();
    });

    function toggleMenu() {
        body.toggleClass(open);
        $('.hamburger').toggleClass('close');
    }
});