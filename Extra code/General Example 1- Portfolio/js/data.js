var sliderContents =
    [{
        id: 0,
        name: 'N.E.C. Hybrid mobile application',
        filename: 'nec',
        description: 'A hybrid mobile application for a soccer club.'
    },
        {
        id: 1,
        name: 'Support System',
        filename: 'supportsystem',
        description: 'A content relationship manager and project planning tool.'
        },
        {
            id: 2,
            name: 'Piixel',
            filename: 'piixel',
            description: 'A responsive blog about lifestyle created with Wordpress.',
            url: '<a href="http://www.piixel.nl" target="_blank" title="Piixel.nl">www.Piixel.nl</a>'
        },
        {
            id: 3,
            name: 'Wijchen Verenigd',
            filename: 'wijchenverenigd',
            description: 'Social Platform to make people more active in Wijchen.'
        }];