<?php
/**
 * The template for displaying comments
 */
if (have_comments()) : ?>
    <h2 class="comments-title">Comments</h2>

    <ol class="comment-list">
        <?php
        wp_list_comments(array(
            'style' => 'ol',
            'short_ping' => true,
            'avatar_size' => 56,
            'reply_text' => '<i class="fa fa-reply"></i> Reply',
        ));
        ?>
    </ol><!-- .comment-list -->
<?php endif; // have_comments()
$comment_args = array(
    'fields' => apply_filters('comment_form_default_fields', array(

        'author' => '<p class="comment-form-author"><input id="author" name="author" type="text" placeholder="Name" aria-required="false"></p>',

        'email' => '<p class="comment-form-email"><input id="email" name="email" type="text" placeholder="E-mail" aria-required="false"></p>',

        'url' => '<p class="comment-form-url"><input id="url" name="url" type="text" placeholder="Website" aria-required="false"></p>')),

    'comment_field' => '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
    'email_notes' => '',
    'comment_notes_after' => ''
);
comment_form($comment_args); ?>
<div class="postEmoticons">
<?php
if (function_exists(cs_print_smilies)) {
    cs_print_smilies();
} ?>
</div>
