<?php get_header(); ?>
<div class="container">
    <!-- AD -->
    <div class="mobile">
        <!-- Top Mobile AD -->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:inline-block;width:320px;height:100px"
             data-ad-client="ca-pub-3617019624363975"
             data-ad-slot="6086171649"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="noMobile">
        <!-- Top non Mobile AD -->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px;"
             data-ad-client="ca-pub-3617019624363975"
             data-ad-slot="9643034042"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <!-- END AD -->
    <div class="content full">
        <?php include 'share.php'; ?>
        <?php while (have_posts()) :
            the_post(); ?>
            <article>
                <h1><?php the_title() ?></h1>

                <?php the_content(); ?>
                <div class="mobile">
                    <!-- AD -->
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Bottom Mobile AD -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:320px;height:100px"
                         data-ad-client="ca-pub-3617019624363975"
                         data-ad-slot="9039638043"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    <!-- END AD -->
                </div>
                <div class="dateContainer">
                                <span class="date"><i class="fa fa-calendar-o"></i> <?php the_time('j M Y') ?> | <i
                                            class="fa fa-comment-o"></i> <?php echo comments_number('0 Comments', '1 Comment', '% Comments'); ?></span>
                </div>

                <!--                        <div class="followMe">-->
                <!--                            <p>Receive a message when there's a new post?</p>-->
                <!--                            <a class="blsdk-follow" href="https://www.bloglovin.com/blogs/piixel-11522025"-->
                <!--                               target="_blank"-->
                <!--                               data-blsdk-type="button" title="Follow Piixel on Bloglovin'">Follow</a>-->
                <!--                        </div>-->

                <!-- AD -->
                <div class="noMobile bottomAD">
                    <!-- Bottom non Mobile AD -->
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:728px;height:90px"
                         data-ad-client="ca-pub-3617019624363975"
                         data-ad-slot="3168548040"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <!-- END AD -->
                <!-- YUZO RELATED POSTS - START -->
                <?php if (function_exists("get_yuzo_related_posts")) {
                    if (has_category() && !in_category('190')) {
                        echo '<div class="otherPosts"><h2>You might also like</h2>';
                        get_yuzo_related_posts();
                        echo '</div>';
                    }
                } ?>
                <!-- YUZO RELATED POSTS - END -->
                <div class="comment">
                    <?php comments_template(); ?>
                </div>
            </article>
            <?php include 'sidebar.php' ?>
        <?php endwhile; ?>
    </div>
</div>
<?php include 'mobileAD.php' ?>
<?php get_footer(); ?>
