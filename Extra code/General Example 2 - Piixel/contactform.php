<?php
/*
 * Template Name: Contact
 */
?>
<?php get_header(); ?>
    <div class="container">
        <?php include 'mobile.php'; ?>
        <!-- AD -->
        <div class="mobile">
            <!-- Top Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="6086171649"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="noMobile">
            <!-- Top non Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px;"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="9643034042"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END AD -->
        <div class="content full">
            <?php while (have_posts()) : the_post(); ?>
                <article>
                    <h1>
                        <?php the_title() ?>
                    </h1>

                    <div class="postContent">
                        <?php the_content(); ?>
                        <div class="contactForm">
                            <form action="http://www.piixel.nl/wp-content/themes/piixel/sendMail.php" method="post"
                                  id="contactForm">
                                <input name="name" type="text" placeholder="Name" required><br><br>
                                <input name="email" type="email" placeholder="E-mail" required><br><br>
                                <textarea name="message" required></textarea>
                                <input name="submit" type="submit" value="Send message">
                            </form>
                        </div>
                        <div id="results"></div>
                    </div>
                </article>
            <?php endwhile; ?>
            <?php include 'sidebar.php' ?>
        </div>
        <!-- AD -->
        <div class="mobile bottomAD">
            <!-- Bottom Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="9039638043"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="noMobile bottomAD topAD">
            <!-- Bottom non Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="3168548040"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END AD -->
    </div>
<?php include 'mobileAD.php' ?>
<?php get_footer(); ?>