<div class="mobile">
    <div class="social">
        <ul>
            <li>
                <a href="http://www.facebook.com/piixel.nl" title="Facebook" target="_blank"><i
                        class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="http://www.instagram.com/piixel.nl" title="Instagram" target="_blank"><i
                        class="fa fa-instagram"></i> </a>
            </li>
            <li>
                <a href="https://www.twitter.com/piixel_nl" title="Twitter" target="_blank"><i
                        class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a href="http://www.bloglovin.com/en/blog/11522025" title="Bloglovin'" target="_blank"><i
                        class="fa fa-heart"></i> </a>
            </li>
        </ul>
    </div>
</div>