<?php get_header(); ?>
    <div class="container">
        <!-- AD -->
        <div class="mobile">
            <!-- Top Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="6086171649"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="noMobile">
            <!-- Top non Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px;"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="9643034042"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END AD -->
        <div class="content main">
            <div>
                <?php while (have_posts()) : the_post(); ?>
                    <?php if (is_search() && ($post->post_type == 'page')) continue; ?>
                    <a href="<?php the_permalink(); ?>"></a>
                        <article class="has-post-thumbnail">
                            <?php the_post_thumbnail(); ?>

                            <div class="postText">

                                <h2><?php the_title() ?></h2>

                                <div class="postContent">
                                    <?php the_content(); ?>
                                </div>
                                <div class="postFooter">
                                    <div class="dateContainer">
                                     <span class="date">
                                         <i class="fa fa-calendar-o"></i>
                                         <?php the_time('j M Y') ?> |
                                             <i class="fa fa-comment-o"></i>
                                         <?php echo comments_number('0 Comments', '1 Comment', '% Comments'); ?>
                                     </span>
                                    </div>
                                </div>
                            </div>
                        </article>

                <?php endwhile; ?>
            </div>
            <!-- AD -->
            <div class="mobile">
                <!-- Bottom Mobile AD -->
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:inline-block;width:320px;height:100px"
                     data-ad-client="ca-pub-3617019624363975"
                     data-ad-slot="9039638043"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <div class="noMobile">
                <!-- Bottom non Mobile AD -->
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:inline-block;width:728px;height:90px"
                     data-ad-client="ca-pub-3617019624363975"
                     data-ad-slot="3168548040"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <!-- END AD -->

            <div class="pageSelector">
                <?php
                posts_nav_link('&nbsp;',
                    '<p class="prev_page"><i class="fa fa-chevron-left"></i> <span>Newer posts</span></p>',
                    '<p class="next_page"><span>Older posts </span><i class="fa fa-chevron-right"></i></p>');
                ?>
            </div>
        </div>
    </div>
<?php include 'mobileAD.php' ?>
<?php get_footer(); ?>