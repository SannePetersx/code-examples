<!--.........................................................-->

<!--Currently redesigning don't mind the HTML.-->
<!-- - Sanne-->

<!--.........................................................-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta property="og:image" content="http://www.piixel.nl/wp-content/themes/piixel/images/websitePreview.png">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700%7COpen+Sans:300,400,700">
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head() ?>
    <link rel="stylesheet" href="http://www.piixel.nl/wp-content/themes/piixel/style.css">
    <link rel="stylesheet" href="http://www.piixel.nl/wp-content/themes/piixel/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(''); ?></title>
    <?php include_once("analyticstracking.php") ?>
    <link rel="icon" href="http://www.piixel.nl/wp-content/themes/piixel/images/favicon.ico?v=3">
    <script type="text/javascript"
            src="<?php bloginfo("template_url"); ?>/js/jquery-1.11.3.min.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="navBar">
    <div class="navBarWrapper">
        <ul class="menuItems">
            <li><a href="http://www.piixel.nl">Home</a></li>
            <li><a href="http://www.piixel.nl/category/lifestyle/">Lifestyle</a></li>
            <li><a href="http://www.piixel.nl/category/fashion/">Style</a></li>
            <li><a href="http://www.piixel.nl/category/beauty/">Beauty</a></li>
            <li><a href="http://www.piixel.nl/category/technology/">Technology</a></li>
        </ul>

        <ul class="social">
            <li>
                <a href="http://www.facebook.com/piixel.nl" title="Facebook" target="_blank"><i
                            class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="http://www.instagram.com/piixel.nl" title="Instagram" target="_blank"><i
                            class="fa fa-instagram"></i> </a>
            </li>
            <li>
                <a href="https://www.twitter.com/piixel_nl" title="Twitter" target="_blank"><i
                            class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a href="http://www.bloglovin.com/en/blog/11522025" title="Bloglovin'" target="_blank"><i
                            class="fa fa-heart"></i> </a>
            </li>
            <li>
                <a href="./contact" title="E-mail'"><i
                            class="fa fa-envelope"></i> </a>
            </li>
            <!--        <li>-->
            <!--            <div id="searchOff">-->
            <!--                <i class="fa fa-search searchIcon" onclick="showSearch()"></i>-->
            <!--            </div>-->
            <!--        </li>-->
            <!--        <div id="searchOn" style="display: none">-->
            <!--            --><?php //get_search_form(); ?>
            <!--        </div>-->
        </ul>
    </div>
</div>
<header>
    <div class="wrapper">
        <div class="logoContainer">
            <a href="http://www.piixel.nl" title="Piixel.nl">
                <div class="logo"></div>
            </a>

            <div class="blogLovin">
                <a class="blsdk-follow" href="https://www.bloglovin.com/blogs/piixel-11522025" target="_blank"
                   data-blsdk-type="button">Follow</a>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://www.bloglovin.com/widget/js/loader.js?v=1";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, "script", "bloglovin-sdk"))</script>
            </div>
        </div>
    </div>
</header>