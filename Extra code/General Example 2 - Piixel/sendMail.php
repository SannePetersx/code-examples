<?php
sendEmail($_POST['name'], $_POST['email'], 'E-Mail van Piixel.nl', $_POST['message'], 'sannepetersx@gmail.com');

function sendEmail($fromName, $fromEmail, $subject, $message, $toEmail)
{
    if (!filter_var($fromEmail, FILTER_VALIDATE_EMAIL)) {
        return json_encode(array("type" => "error", "msg" => "Please enter a valid e-mail address."));
    }
    if (trim($fromName) == "") {
        return json_encode(array("type" => "error", "msg" => "Please enter your name."));
    }
    if (trim($message) == "") {
        return json_encode(array("type" => "error", "msg" => "Please enter a message."));
    }

    $emailMessage = "E-Mail van wwww.Piixel.nl:<br><br>";
    $emailMessage .= "Naam: " . $fromName . "<br>";
    $emailMessage .= "E-mailadres: " . $fromEmail . "<br>";
    $emailMessage .= "Bericht: " . $message . '<br>';
    $emailMessage .= "IP: " . $_SERVER['REMOTE_ADDR'];

    // To send HTML mail, the Content-type header must be set
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();
    $headers .= "Reply-to: $fromEmail";


    mail($toEmail, $subject, $emailMessage, $headers);
    header("Location: http://www.piixel.nl/contact-message-send");
}
?>