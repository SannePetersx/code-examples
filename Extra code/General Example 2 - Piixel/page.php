<?php get_header(); ?>
    <div class="container">
        <!-- AD -->
        <div class="mobile">
            <!-- Top Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="6086171649"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="noMobile">
            <!-- Top non Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px;"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="9643034042"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END AD -->
        <div class="content full">
            <?php while (have_posts()) : the_post(); ?>
                <article>
                    <h1>
                        <?php the_title() ?>
                    </h1>

                    <div class="postContent">
                        <?php the_content(); ?>
                    </div>
                </article>
            <?php endwhile; ?>
            <?php include 'sidebar.php' ?>
        </div>
        <!-- AD -->
        <div class="mobile bottomAD">
            <!-- Bottom Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="9039638043"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="noMobile bottomAD">
            <!-- Bottom non Mobile AD -->
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px"
                 data-ad-client="ca-pub-3617019624363975"
                 data-ad-slot="3168548040"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END AD -->
    </div>
<?php include 'mobileAD.php' ?>
<?php get_footer(); ?>