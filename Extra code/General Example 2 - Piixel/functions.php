<?php
register_sidebar(array(
    'name' => 'sidebar1',
    'id' => 'leftsidebar1',
    'before widget' => '<div class="widget">',
    'after widget' => '</div>'
));

register_sidebar(array(
    'name' => 'sidebar2',
    'id' => 'leftsidebar2',
    'before widget' => '<div class="widget">',
    'after widget' => '</div>'
));

register_sidebar(array(
    'name' => 'sidebar3',
    'id' => 'leftsidebar3'
));

//Enable more buttons in admin panel
function enable_more_buttons($buttons)
{
    $buttons[] = 'hr';
    $buttons[] = 'sub';
    $buttons[] = 'sup';
    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    $buttons[] = 'cleanup';
    $buttons[] = 'styleselect';

    return $buttons;
}

add_filter("mce_buttons_3", "enable_more_buttons");

// Custom avatar for users without one
add_filter('avatar_defaults', 'newgravatar');
function newgravatar($avatar_defaults)
{
    $myavatar = get_bloginfo('template_directory') . '/images/avatar-default.png';
    $avatar_defaults[$myavatar] = "Own";
    return $avatar_defaults;
}

// Read More
add_filter('the_content_more_link', 'modify_read_more_link');
function modify_read_more_link()
{
    return '<br><a href="' . get_permalink($post->ID) . '" class="readMoreWrapper" title="Read more"><button class="readMore">Read more</button></a>';
//    return '<a href="' . get_permalink($post->ID) . '" class="" title="Read more">... Click to read more ...</a>';
}

// Choose post thumbnail
add_theme_support('post-thumbnails');

// Add title and alt to gravatar
function replace_content($text)
{
    $alt = get_comment_author('ID');
    $text = str_replace('alt=\'\'', 'alt=\'' . $alt . '\'', $text);
    return $text;
}

function piixel_jpeg_quality_callback($arg)
{
    return (int)100;
}

add_filter('jpeg_quality', function ($arg) {
    return 100;
});

// Exclude category 'excl_posts' from homepage
function exclude_category($query)
{
    if ($query->is_home() && $query->is_main_query()) {
        $query->set('cat', '-190');
    }
}

add_action('pre_get_posts', 'exclude_category');

// Exclude drafts from 'all' posts in WP_Admin
add_action('init', function () use (&$wp_post_statuses) {
    $wp_post_statuses['draft']->show_in_admin_all_list = false;
//    $wp_post_category['190']->show_in_admin_all_list = false;
}, 1);
?>