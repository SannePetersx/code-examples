//Share on social media
function share(type) {
    var winWidth = 520;
    var winHeight = 350;
    var winTop = (screen.height / 2) - (winWidth / 2);
    var winLeft = (screen.width / 2) - (winHeight / 2);
    var url = window.location.href;
    var content = document.title;

    // Replace space through %20
    content = encodeURIComponent(content.trim());

    if (type == 'facebook') {
        window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    } else if (type == 'twitter') {
        window.open('https://twitter.com/share?text=' + content, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    } else if (type == 'gplus') {
        window.open('https://plus.google.com/share?url=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    } else {
        console.log('NO SOCIAL MEDIA SHARE TYPE FOUND');
    }
};

//Show searchbar
function showSearch() {
    //document.getElementById('searchOn').style.display = 'block';
    //document.getElementById('searchOff').style.display = 'none';
    $("#searchOn").toggle();
}

function showCategories() {
    // Showing categories
    var categories;
    categories = document.getElementById('categories');

    if (document.getElementById('categories').style.display == 'block') {
        categories.style.display = 'none';
        menuStyling(false);

    } else if (document.getElementById('categories').style.display == 'none') {
        categories.style.display = 'block';
        menuStyling(true);
    }
};

function menuStyling(open) {
    // Category styling when opening/closing
    var t, c;
    t = document.getElementsByClassName('menuBar')[0];
    c = document.getElementById('categoryItem');
    if (open == true) {
        t.className = t.className + ' ' + t.className + '--open';
        c.className = c.className + ' ' + c.className + '--open';
    } else {
        t.className = 'menuBar';
        c.className = 'categoryItem';
    }
}

// If post has thumbnail delete all images from post
$(function () {
    $('.hasThumbnail p').each(function () {
        if ($(this).find('img').length) {
            this.style.display = 'none';
        }
    });
});

// Bloglovin' subscribe by e-mail & button
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://widget.bloglovin.com/assets/widget/loader.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "bloglovin-sdk"));

// Make article clickable
$(function () {
    $("article.has-post-thumbnail").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });
});