<?php dynamic_sidebar('sidebar2'); ?>
<footer>
    <div class="footerWrapper">
        <!--Follow by email on Bloglovin'-->
        <div class="subscribeEmail">
            <a class="blsdk-follow" href="https://www.bloglovin.com/blogs/piixel-11522025" target="_blank"
               style="border:none!important;"
               title="Follow Piixel on Bloglovin'">Follow</a>
        </div>
        <!--Instagram-->
        <div class="footerNav">
            <h2>Blog</h2>
            <ul>
                <li><a href="/about" title="About">About</a></li>
                <li><a href="/advertising/" title="Partners">Advertising</a></li>
                <li><a href="http://feeds.feedburner.com/feedburner/kNhXM" target="_blank" title="RSS">RSS</a></li>
                <li><a href="/sitemap_index.xml" target="_blank" title="Sitemap">Sitemap</a></li>
                <li><a href="/contact" title="Contact">Contact</a></li>
            </ul>
        </div>
        <!--Bloglinks-->
        <div class="footerSocial">
            <h2>Follow me</h2>
            <div class="follow">

                <ul>
                    <li>
                        <a href="http://www.facebook.com/piixel.nl" title="Facebook" target="_blank"><i
                                    class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.instagram.com/piixel.nl" title="Instagram" target="_blank"><i
                                    class="fa fa-instagram"></i> </a>
                    </li>
                    <li>
                        <a href="https://www.twitter.com/piixel_nl" title="Twitter" target="_blank"><i
                                    class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="http://www.bloglovin.com/en/blog/11522025" title="Bloglovin'" target="_blank"><i
                                    class="fa fa-heart"></i> </a>
                    </li>
                    <li><a href="http://www.pinterest.com/piixel_nl" title="Pinterest" target="_blank"><i
                                    class="fa fa-pinterest"></i></a></li>
                    <li><a href="https://plus.google.com/b/112946124464380705206/+PiixelNlBlog" target="_blank"
                           title="Google+"><i
                                    class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="copy">&copy; 2013 - 2017 <a href="http://www.sannepeters.nl" title="Portfolio" target="_blank">Made
                by me</a>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/function.js"></script>
<div>
    <?php wp_footer(); ?>
</div>
</body>
</html>
