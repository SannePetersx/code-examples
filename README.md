Code Examples voor BigBridge
=====

Hey Pascal,

Hier zijn wat code examples die uit diverse projecten zijn getrokken.

SCSS Example - Planbord: Dit was voor een CRM systeem met SCRUM planbord waarbij gebruik werd gemaakt van sockets en Smarty(PHP Templating engine). Met SASS ben ik pas sinds kort bezig en heb ik nog niet echt een goed voorbeeld van. Deze code zit achter systeem + planbord dat te zien is op deze pagina:
http://www.sannepeters.nl/projects/supportsystem.php

JQuery Example - Planbord: Dit was de JQuery die voor dat planboard gebruikt werd. Ik heb ook even de templates bijgevoegd om het wat duidelijker te maken. Deze code zit achter het planbord op de 3e screenshot van deze pagina: http://www.sannepeters.nl/projects/supportsystem.php

JQuery Example 2 - Portfolio: Dit is de jQuery code die gebruikt wordt voor  http://www.sannepeters.nl

Extra code:

General Example 1 - Portfolio: Dit is alle code die gebruikt wordt voor http://www.sannepeters.nl
General Example 2 - Piixel: Dit is alle code die gebruikt wordt voor http://www.piixel.nl (deze code is het oudste en slordigste omdat er WordPress heen is gewerkt en dit de eerste keer was met SCSS)
